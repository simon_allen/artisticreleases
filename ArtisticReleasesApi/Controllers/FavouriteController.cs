﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ArtisticReleasesApi;

namespace ArtisticReleasesApi.Controllers
{
    public class FavouriteController : ApiController
    {
        private masterEntities db = new masterEntities();

        // get all favourites for user
        // GET: api/Favourite/user_id
        [ResponseType(typeof(IEnumerable<favourite>))]
        public IHttpActionResult GetFavouritesForUser(int id)
        {
            List<favourite> favourites = db.favourites.Where(f => f.user_id.Equals(id)).ToList();
            return Ok(favourites);
        }

        // PUT: api/Favourite/5 - dont really need this but can leave it in
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFavourite(int id, favourite favourite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != favourite.favourite_id)
            {
                return BadRequest();
            }

            db.Entry(favourite).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavouriteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // add favourites
        // POST: api/Favourite
        [ResponseType(typeof(favourite))]
        public IHttpActionResult PostFavourite(favourite favourite)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.favourites.Add(favourite);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = favourite.favourite_id }, favourite);
        }

        // DELETE: api/Favourite/5
        [ResponseType(typeof(favourite))]
        public IHttpActionResult DeleteFavourite(int id)
        {
            favourite favourite = db.favourites.Find(id);
            if (favourite == null)
            {
                return NotFound();
            }

            db.favourites.Remove(favourite);
            db.SaveChanges();

            return Ok(favourite);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavouriteExists(int id)
        {
            return db.favourites.Count(e => e.favourite_id == id) > 0;
        }
    }
}