﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ArtisticReleasesApi;

namespace ArtisticReleasesApi.Controllers
{
    [EnableCors(origins: "http://localhost:9876, http://localhost:4200", headers: "*", methods: "*")]
    public class ArtistController : ApiController
    {
        private masterEntities db = new masterEntities();

        // GET: api/Artist/searchvalue
        [ResponseType(typeof(List<artist>))]
        [HttpGet]
        // leaving parameter name as id to avoid having to write another route
        public IHttpActionResult SearchArtists(string id)
        {
            string searchTerm = id;
            List<artist> artists = db.artists.Where(a => a.artist_name.Contains(searchTerm)).ToList();
            return Ok(artists);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtistExists(int id)
        {
            return db.artists.Count(e => e.artist_id == id) > 0;
        }
    }
}