﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ArtisticReleasesApi;

namespace ArtisticReleasesApi.Controllers
{
    public class ReleaseController : ApiController
    {
        private masterEntities db = new masterEntities();

        // GET: api/releases
        [ResponseType(typeof(IEnumerable<release>))]
        [HttpGet]
        public IHttpActionResult GetReleasesForArtist(int artist_id)
        {
            List<release> releases = db.releases.Where(r => r.artist_id.Equals(artist_id)).ToList();
            return Ok(releases);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReleaseExists(int id)
        {
            return db.releases.Count(e => e.release_id == id) > 0;
        }
    }
}