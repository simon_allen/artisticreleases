﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtisticReleasesApi.Models
{
    public class Artist
    {
        public int artist_id { get; set; }
        public string artist_name { get; set; }
    }
}