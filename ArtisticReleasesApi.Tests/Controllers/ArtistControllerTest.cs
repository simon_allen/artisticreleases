﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArtisticReleasesApi;
using ArtisticReleasesApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections;
using System.Collections.Generic;

namespace ArtisticReleasesApi.Tests.Controllers
{
    [TestClass]
    public class ArtistControllerTest
    {
        [TestMethod]
        public void TestMethodSearchForArtistReturnOne()
        {
            ArtistController controller = new ArtistController();

            IHttpActionResult actionResult = controller.SearchArtists("Figure");

            var contentResult = actionResult as OkNegotiatedContentResult<List<artist>>;

            Assert.IsNotNull(contentResult);
            List<artist> content = contentResult.Content;
            Assert.AreEqual(1, content.Count);
            artist artist1 = content[0];
            Assert.AreEqual("Figurehead", artist1.artist_name);
        }

        [TestMethod]
        public void TestMethodSearchForArtistReturnMany()
        {
            ArtistController controller = new ArtistController();

            IHttpActionResult actionResult = controller.SearchArtists("e");

            var contentResult = actionResult as OkNegotiatedContentResult<List<artist>>;

            Assert.IsNotNull(contentResult);
            List<artist> content = contentResult.Content;
            Assert.AreEqual(2, content.Count);
            artist artist1 = content[0];
            Assert.AreEqual("Figurehead", content[0].artist_name);
            Assert.AreEqual("Metallica", content[1].artist_name);
        }
        [TestMethod]
        public void TestMethodSearchForArtistReturnNone()
        {
            ArtistController controller = new ArtistController();

            IHttpActionResult actionResult = controller.SearchArtists("xxx");

            var contentResult = actionResult as OkNegotiatedContentResult<List<artist>>;

            Assert.IsNotNull(contentResult);
            List<artist> content = contentResult.Content;
            Assert.AreEqual(0, content.Count);
        }

    }
}
