import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ArtistService } from './artist.service';
import { Artist } from './shared/artist';

describe('ArtistService', () => {
  let service: ArtistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(ArtistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a search match', () => {
    var promise: Promise<Array<Artist>> = service.searchArtist("Figure");
    expect(promise).toBeDefined();
    promise.then(data => {
      expect(data.length).toEqual(1);
      expect(data[0].artist_name).toEqual("Figurehead");
    }, err => {
      console.log(err);
      expect(null).toBeDefined();
    });
  })

  it('should call the webapi and return multiple search matches', () => {
    var promise: Promise<Array<Artist>> = service.searchArtist("e");
    expect(promise).toBeDefined();
    promise.then(data => {
      expect(data.length).toEqual(2);
      expect(data[0].artist_name).toEqual("Figurehead");
      expect(data[1].artist_name).toEqual("Metallica");
    }, err => {
      console.log(err);
      expect(null).toBeDefined();
    });
  })

});
