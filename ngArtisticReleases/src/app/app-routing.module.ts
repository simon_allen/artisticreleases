import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { MaintainFavouritesComponent } from './maintain-favourites/maintain-favourites.component';
import { SearchArtistComponent } from './search-artist/search-artist.component';

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  { path: "home", component: HomeComponent },
  { path: "maintain-favourites", component: MaintainFavouritesComponent },
  { path: "search-artist", component: SearchArtistComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
