import { Component, OnInit } from '@angular/core';

import { ArtistService } from '../artist.service';
import { Artist } from '../shared/artist';

@Component({
  selector: 'app-search-artist',
  templateUrl: './search-artist.component.html',
  styleUrls: ['./search-artist.component.css']
})
export class SearchArtistComponent implements OnInit {
  searchTerm: string;
  artistSearchResults: Array<Artist> = new Array<Artist>();
  constructor(private service: ArtistService) { }

  ngOnInit(): void {
  }

  searchArtists(searchTerm: string) {
    console.log("searchArtists:" + searchTerm);
    var promise: Promise<Array<Artist>> = this.service.searchArtist(searchTerm);
    promise.then(data => {
      this.artistSearchResults = data;
    }, err => {
        console.log(err);

    });
  }

}
