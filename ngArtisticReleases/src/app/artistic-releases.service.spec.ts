import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { Artist } from './shared/artist';
import { ArtisticReleasesService } from './artistic-releases.service';

describe('ArtisticReleasesService', () => {
  let service: ArtisticReleasesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(ArtisticReleasesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the webapi and return a search match', () => {
    var promise: Promise<Array<Artist>> = service.getApi("/Artist/Figure");
    expect(promise).toBeDefined();
    promise.then(data => {
      expect(data.length).toEqual(1);
      expect(data[0].artist_name).toEqual("Figurehead");
    }, err => {
      console.log(err);
      expect(null).toBeDefined();
    });
  })

  it('should call the webapi and return multiple search matches', () => {
    var promise: Promise<Array<Artist>> = service.getApi("/Artist/e");
    expect(promise).toBeDefined();
    promise.then(data => {
      expect(data.length).toEqual(2);
      expect(data[0].artist_name).toEqual("Figurehead");
      expect(data[1].artist_name).toEqual("Metallica");
    }, err => {
        console.log(err);
        expect(null).toBeDefined();
    });
  })
});
