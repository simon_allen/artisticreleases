import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Artist } from './shared/artist';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})

export class ArtisticReleasesService {

  endpointBase = environment.artisticReleasesUrlBase;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  async getApi(apiPart: string) {
    return await this.http.get<Array<Artist>>(this.endpointBase + apiPart).toPromise<Array<Artist>>();
  }
}
