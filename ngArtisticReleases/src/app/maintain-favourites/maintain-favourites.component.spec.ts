import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintainFavouritesComponent } from './maintain-favourites.component';

describe('MaintainFavouritesComponent', () => {
  let component: MaintainFavouritesComponent;
  let fixture: ComponentFixture<MaintainFavouritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintainFavouritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintainFavouritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
