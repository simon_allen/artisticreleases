import { Injectable } from '@angular/core';
import { ArtisticReleasesService } from './artistic-releases.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(private service: ArtisticReleasesService) { }

  searchArtist(searchTerm: string) {
    return this.service.getApi("/Artist/" + encodeURI(searchTerm));
  }
}
